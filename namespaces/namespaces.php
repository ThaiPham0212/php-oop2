<?php

namespace namespaces\ConNguoi;

class ConNguoi
{
    private $name = 'Con Người';

    public function getName()
    {
        return $this->name;
    }
}

namespace namespaces\NguoiLon;

class NguoiLon
{
    private $name = 'Nguoi Lon';

    public function getName()
    {
        return $this->name;
    }
}