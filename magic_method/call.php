<?php

class ConNguoi
{
    private $name = "Pham Hong Thai";
    private $age = 22;

    public function __call($methodName, $arguments)
    {
        echo 'Bạn vừa gọi phương thức: ' . $methodName . ' và có các tham số: ' . implode('-', $arguments);
    }

    private function getInfo()
    {
        echo $this->name . ' + ' . $this->age;
    }
}

$connguoi = new ConNguoi();
echo "<br/>";
//Khi không truyền tham số.
$connguoi->getInfo();
//Kết quả: Bạn vừa gọi phương thức: getInfo và có các tham số:
echo "<br/>";
//Khi truyền tham số
$connguoi->getInfo('name', 'age');
//Kết quả: Bạn vừa gọi phương thức: getInfo
//và có các tham số: Bạn vừa gọi phương thức: getInfo và có các tham số: name-age