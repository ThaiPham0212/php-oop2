<?php

class ConNguoi2
{
    private static $name = 'amonymouse';

    public function setName($name)
    {
        self::$name = $name;
    }
    public function getNameAgain()
    {
        return ConNguoi2::getName();
    }

    public static function getName()
    {
        return ConNguoi2::$name;
    }
}

//khởi tạo đối tượng con người
$chuBlog = new ConNguoi2();
//set name cho đối tượng
$chuBlog->setName('Vũ Thanh Tài');
//in ra name của đối tượng
echo $chuBlog->getName();
//kết quả: Vũ Thanh Tài
//khởi tạo đối tượng Con người
$nguoixem = new ConNguoi2();
//hiển thị ra tên người xem
echo $nguoixem->getNameAgain();

//Kết quả: Vũ Thanh Tài
class NguoiLon extends ConNguoi2
{
    public function getNameAgain()
    {
        parent::getName();
    }
}