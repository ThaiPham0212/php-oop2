<?php

class ConNguoi
{
    public $name = "Pham Hong Thai";
    public $age = 21;

    public function __clone()
    {
        echo 'Phương thức __clone() được gọi';
    }
}

$connguoi = new ConNguoi();

$connguoi2 = clone $connguoi;
// Phương thức __clone() được gọi

echo $connguoi2->name;
// Pham Hong Thai