<?php

class ConNguoi
{
    public $name;

    public function __set($key, $value)
    {
        die('Phương thức __set() được gọi');
    }

    public function getName()
    {
        echo $this->name;
    }
}

$connguoi = new ConNguoi();

/*gọi thuộc tính được phép truy cập*/
$connguoi->name = "Vũ Thanh Tài";
$connguoi->getName();
//Vũ Thanh Tài

/*gọi thuộc tính được phép truy cập*/
$connguoi->age = 20;
//Phương thức __set() được gọi