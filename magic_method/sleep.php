<?php

class ConNguoi
{
    private $name = 'Pham Hong Thai';
    private $age = 20;

    public function __sleep()
    {
        //trả về thuộc tính name
        return array('name');
    }
}

echo serialize(new ConNguoi());
//O:8:"ConNguoi":1:{s:4:"name";s:14:"Pham Hong Thai";}