<?php

class ConNguoi
{
    private $name;

    public function __unset($name)
    {
        echo 'Bạn vừa hủy thuộc tính: ' . $name;
    }
    public function getName(){
        return $this->name;
}
}

//Khởi tạo đối tượng
$connguoi = new ConNguoi();

unset($connguoi->name);
//Kết quả: Bạn vừa hủy thuộc tính: name
echo "<br/>";
/* unset thuộc tính không tồn tại trong đối tượng*/
unset($connguoi->age);
//Kết quả: Bạn vừa hủy thuộc tính: age
