<?php

class ConNguoi
{
    private static $name;

    public function __isset($name)
    {
        echo 'Bạn vừa kiểm tra thuộc tính: ' . $name;
    }
}

//Khởi tạo đối tượng
$connguoi = new ConNguoi();

isset($connguoi->name);
//Kết quả: Bạn vừa kiểm tra thuộc tính: name
echo "<br/>";
empty($connguoi->name);
//Kết quả Bạn vừa kiểm tra thuộc tính: name
echo "<br/>";
/*kiểm tra thuộc tính không tồn tại trong đối tượng*/
isset($connguoi->age);
//Kết quả: Bạn vừa kiểm tra thuộc tính: age