<?php

class ConNguoi
{
    private $name = "Pham Hong Thai";
    private $age = 22;

    public static function __callStatic($methodName, $arguments)
    {
        echo 'Bạn vừa gọi phương thức: ' . $methodName . ' và có các tham số: ' . implode('-', $arguments);
    }

    private static function getInfo()
    {
        echo $this->name . ' + ' . $this->age;
    }
}

ConNguoi::getInfo();
//Kết Quả: Bạn vừa gọi phương thức: getInfo và có các tham số:
echo "<br/>";
/*Khi truyền tham số*/
ConNguoi::getInfo('name', 'age');
//Kết quả: Bạn vừa gọi phương thức: getInfo và có các tham số: name-age