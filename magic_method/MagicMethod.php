<?php

class MagicMethod
{
    public $smg;

    public function __construct($text)
    {
        echo $text;
    }

    public function __destruct()
    {
        echo "<br/>";
        echo "<br/>";
        echo "Huy doi tuong";
    }

    public function __get($key)
    {
        echo "ban vua goi den thuoc tinh khong ton tai trong class: " . $key;
    }

    public function __set($key, $value)
    {
        echo "$key";
        echo "<br/>";
        echo "$value";
    }

    public function __isset($name)
    {
        echo "thuoc tinh kiem tra khong ton tai: " . $name;
    }

    public function __unset($name)
    {
        echo "Thuoc tinh unset khong ton tai:" . $name;
    }

}

$varGet = new MagicMethod('hellooo');
echo "<br/>";
$varGet->sc;
echo "<br/>";
$tweet = new MagicMethod('Hello');
echo "<br/>";
$varSet = new MagicMethod('hello');
echo "<br/>";
$varSet->name = 'Pham Hong Thai';
echo "<br/>";
isset($varGet->name);
echo "<br/>";
unset($varGet->name);
echo "<br/>";
echo "<br/>";
echo "<br/>";

class test
{
    public function __call($method_name, $parameter)
    {
        if ($method_name == "overloadFunction") {
            $count = count($parameter);
            switch ($count) {
                case "1":
                    echo "viet code cho 1 doi so";
                    break;
                case "2":
                    echo "viet code cho 2 doi so";
                    break;
                default:
                    throw new exception("Bad argument");
            }
        } else {
            throw new exception("Function $method_name doesn't exists");
        }
    }
}

$a = new test();
$a->overloadFunction("1");
echo "<br/>";
$a->overloadFunction("1", "2");
echo "<br/>";
echo "<br/>";
echo "<br/>";

class ConNguoi
{
    public static function __callStatic($methodName, $arguments)
    {
        echo 'ban vua goi phuong thuc: ' . $methodName . ' va co cac tham so: ' . implode(' - ', $arguments);
    }

}

ConNguoi::getInfo('name', 'age');
echo "<br/>";
echo "<br/>";
echo "<br/>";

class A
{
    public function __toString()
    {
        echo "ban echo 1 doi tuong";
        echo "<br/>";
        return "Return ve string";
    }

    public function __invoke($name)
    {
        echo $name;
    }
}

$aToString = new A;
echo $aToString;
$a = new A;
echo "<br/>";
$a('Pham Hong Thai');
echo "<br/>";
echo "<br/>";
echo "<br/>";

class B
{
    private $name = 'B';
    private $age = '22';

    public function __sleep()
    {
        return array('name');
    }

    public function __wakeup()
    {
        echo "__wakeup";
    }
}

echo serialize(new B());
echo "<br/>";
unserialize(serialize(new B));
echo "<br/>";
echo "<br/>";
echo "<br/>";

class C{
    public static function __set_state($var){
        echo "__set_state";
    }
}

$varC = new C();
var_export($varC);
echo "<br/>";
echo "<br/>";
echo "<br/>";
class D{
    public function __clone(){
        echo "Doi tuong vua duoc clone";
    }
}
$varD = new D();
$varD2 = clone $varD;
echo "<br/>";
echo "<br/>";
echo "<br/>";

class E{
    public function __debugInfo(){
        echo "Debug!!";
    }
}
$varE = new E();
var_dump($varE);