<?php

class ConNguoi
{
    private $name = 'Pham Hong Thai';
    private $age = 20;

    public function __invoke()
    {
        echo 'Phương thức __invoke() được gọi';
    }
}

$congnuoi = new ConNguoi();

$congnuoi();
//Kết quả: Phương thức __invoke() được gọi